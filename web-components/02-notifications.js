class NotificationElement extends HTMLElement {
    constructor () {
        super()
        this.template = document.getElementById("notification")
        this.notification = document.importNode(this.template.content,true)
    }
    connectedCallback () {
        // let clonedDom = document.importNode(this.template.content, true)
        this.appendChild(this.notification)
    }
    disconnectedCallback() {}
    attributeChangedCallback (attr, oldValue, newValue) {
        if (attr === 'title') {
            this.notification.querySelector('.error__title').innerText = newValue
        }
    }
    static get observedAttributes () {
        return ['title']
    } 
}

window.customElements.define('notification-element', NotificationElement)