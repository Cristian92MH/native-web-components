class SaludoBasicoElement extends HTMLElement  {
    constructor () {
        super()
        this.saludo = 'Hola como estas'
        this.pintado = false;
    }
    connectedCallback() {
        this.pintado = true
        this.innerHTML = this.saludo
        // let bold = document.createElement("strong")
        // bold.innerHTML = this.saludo
        // this.appendChild(bold)
    }
    disconnectedCallback () {
        this.pintado = false
    }
    attributeChangedCallback (nameAttribute, oldValue, newValue) {
        // console.log(`${newValue} ha cambiado del valor anterior ${oldValue}`)
        if (nameAttribute === 'nombre' && newValue !== oldValue && this.pintado) {
            this.saludo = `Hola, ${newValue}`
            this.innerHTML = this.saludo
        }     
    } 
    static get observedAttributes() {
        return ['nombre']
    }
    disconnectedCallback() { } 
}
window.customElements.define('saludo-basico', SaludoBasicoElement)