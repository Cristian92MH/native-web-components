// document.getElementById('save').addEventListener('click', () => {
//     console.log('hola');
    
//     document.getElementById('save').innerHTML = "Guardando ..."
//     document.getElementById('save').setAttribute('disabled', 'disabled')
// })

class desactivableButton extends HTMLButtonElement {
    constructor () {
        super() 
        this.busyText = "Guardando... "
        this._onButtonClicked = this._onButtonClicked.bind(this)
    }
    _onButtonClicked () {
        this.setAttribute('disabled', 'disabled')
        this.innerText = this.busyText
    }
    connectedCallback () {
        this.addEventListener('click', this._onButtonClicked)
    }
    disconnectedCallback(){
        this.removeEventListener('click', this._onButtonClicked)
    }
    static get observedAttributes () {
        console.log("HOLA");
        return ['busytext']
    }
    attributeChangedCallback (attribute, oldValue, newValue) {        
        if (attribute == 'busytext') {
             this.busyText = newValue            
        }
    }
}

window.customElements.define('desactivable-button', desactivableButton,
{extends: "button"}
)