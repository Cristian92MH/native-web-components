class NotificationZero extends HTMLElement {
    constructor () {
        super()
        this.attachShadow({mode: 'open'})
        this.template = document.getElementById("notification")
        this.notification = document.importNode(this.template.content,true)
    }
    connectedCallback () {
        this.shadowRoot.appendChild(this.notification)
    }
    disconnectedCallback () {}
    attributeChangedCallback (attribute, oldValue, newValue) {
        if (attribute === 'title') {
            this.notification.querySelector('.error__title').innerText = newValue
        }
    }
    static get observedAttributes () {
        return ['title']
    }
}
window.customElements.define('notification-zero', NotificationZero)